model regulator

import '../models/Main.gaml'
import './Crop.gaml'
import './Field.gaml'

species Regulator {
		int max_simultaneous_crops <- 3;//
		float water_right_agriculture <- 0.7; // Water Right percentage for irrigation purposes = x% of total water supply
		int max_consecutive_crops <- 3; //
		float coop_incentive <- 1 as float; //Financial incentive for farmers to cooperate 
		float upper_limit; // Threshold for updating cooperation incentive
		float monthly_water_supply;		
		

	reflex update_coop_incentive when: (monthly_water_supply >= upper_limit){//TODO
		float coop_incentive <- coop_incentive*1.1; // Arbitrary increase number
	}

}
