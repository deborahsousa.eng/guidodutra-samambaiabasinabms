model farmer

import './Crop.gaml'
import './Field.gaml'

species Farmer {
	float cash;
	float initial_month_cash <- 0;
	float monthly_cash;
	list<Crop> current_crops;
	list<Crop> previous_crops;
	list<Field> fields;
	int number_of_fields;
	int initial_sow_day <- rnd(30);
	float price_tolerance;
	int max_simultaneous_crops <- 3;
	list private_data;
	float daily_water_usage;
	float monthly_water_usage;
	float my_area;
	float coop_incentive; //from Regulator
	int coop_number; // number of cooperation transactions
	float single_coop_cost <- 1.0 ;
	float total_coop_cost ; // cost of all cooperation transactions
	
aspect {
		draw envelope(fields);
	}
	
	reflex update_crop when: (cycle >= initial_sow_day and empty(current_crops)) {	
		list<Crop> available_crops;
		list<Crop> not_previously_planted <- crops;
		loop previous over: previous_crops {
			remove previous from: not_previously_planted;
		}
		loop crop over: not_previously_planted {
			if (crop.available_months contains month_of_year) {
				add crop to: available_crops;
			}
		}
		
		loop while: should_plant_more() {
			Crop selected_crop <- get_highest_profit_crop(available_crops);
			if (selected_crop = nil) {
				break;
			}
			add selected_crop to: current_crops;
			remove selected_crop from: available_crops;				
		}
		
		previous_crops <- current_crops;
		
		if (!empty(current_crops)) {
			int crop_count <- length(current_crops);
			int i <- 0;
			int j <- 0;
			int fields_per_crop <- length(fields) / crop_count as int;
			if crop_count > 1 {
				loop times: crop_count - 1 {
					list<Field> crop_fields <- copy_between(fields, j, j + fields_per_crop);
					ask crop_fields {
						do create_new_harvest(myself.current_crops[i]);
					}
					j <- j + fields_per_crop;
					i <- i + 1;
				}				
			}
			list<Field> last_crop_fields <- copy_between(fields, j, length(fields));
			ask last_crop_fields {
				do create_new_harvest(myself.current_crops[i]);
			}			
		}
		
	}
		
	reflex daily_payment {
		float daily_costs <- 0.0;
		loop field over: fields {
			if (field.current_harvest != nil) {
				cash <- cash - field.daily_cost;	
			}
		}
		daily_water_usage <- sum(collect(fields, each.water_demand));
		monthly_water_usage <- monthly_water_usage + daily_water_usage;
	}
	
	reflex save_monthly_data{
			if (current_date.day = 1){
				monthly_cash <- initial_month_cash - cash;
				list<float> new_monthly_crop_area <- list_with(8, 0.0);
				loop field over: fields {
					if (field.current_harvest != nil and field.current_harvest.crop != nil) {
						int crop_number <- field.current_harvest.crop.crop_number;
						new_monthly_crop_area[crop_number] <- new_monthly_crop_area[crop_number] + field.shape.area;
					}				
				}		 
			private_data <<+ [ year_count, current_date.month, monthly_cash, monthly_water_usage, new_monthly_crop_area]; // TODO produção mensal, area de tipo de produção
			initial_month_cash <- cash;
			monthly_water_usage <-0 ;			 
		}
	}
	
	
	action should_plant_more type: bool {
		return !is_within_tolerance(current_crops) and length(current_crops) < max_simultaneous_crops;
	}
	
	action is_within_tolerance(list<Crop> selected_crops) type: bool {
		if empty(selected_crops) { return false; }
		float fluctuation <- product_of(selected_crops, monthly_prices[46, each.crop_number] as float);
		return fluctuation < price_tolerance;
	}
	
	action get_highest_profit_crop(list<Crop> available_crops) type: Crop {
		float current_expected_profit <- 0.0;
		Crop result <- nil;
		loop crop over: available_crops {
			float crop_expected_profit <- crop.calculate_expected_profit();
			if (crop_expected_profit > current_expected_profit) {
				current_expected_profit <- crop_expected_profit;
				result <- crop;
			}
		}
		return result;
	}
	
	reflex update_coop_cost{
		total_coop_cost <- (coop_number*single_coop_cost)*coop_incentive;
	}
}
	/*action cooperate type: bool {//TODO
		if farmer_communication >= 0 {
			return true;
		}
	}*/
	
	//reflex irrigate when: crop_water_demand <= expected_rain {
	//} //TODO
			
	/*action cooperate type: bool {//TODO
		if monthly_cash_data > monthly_cash_data {
		return true;		
		}
	}*/






