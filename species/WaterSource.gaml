model WaterSource

import '../models/Main.gaml'
import './Regulator.gaml'

species WaterSource {
	float daily_supply; //dayly limit to withdraw (Water right * Q95 of the day)
	float daily_demand update: calculate_daily_demand();
	float total_demand update: total_demand + daily_demand;
	float monthly_demand;
	float monthly_supply;
	list<float> monthly_crop_area update: monthly_production();

	
	matrix flow_data;
	list<Field> irrigated_fields;
	action calculate_daily_demand type: float {
		if (irrigated_fields = nil) { return 0; }
		return sum(collect(irrigated_fields, each.water_demand));
		
	}
	reflex calculate_daily_supply {
		float monthly_flow <- flow_data[1, month_of_year] as float;
		daily_supply <- (monthly_flow * shape.area * 0.0864 * water_right_agriculture) - daily_demand;
	}
	reflex monthy_data_calculator { //TODO
		monthly_demand <- 0;	
		float monthly_flow <- flow_data[1, month_of_year] as float;
		monthly_supply <-  (monthly_flow * shape.area / 1000000) - monthly_demand;	
		if (current_date.day = 1){	
			monthly_demand <- 0;
			monthly_supply <- 0;
		}
	}
	action monthly_production {
		if (current_date.day = 1 and irrigated_fields != nil){
			list<float> new_monthly_area_per_crop <- list_with(8, 0.0);
			loop field over: irrigated_fields {
				if (field.current_harvest != nil and field.current_harvest.crop != nil) {
					int crop_number <- field.current_harvest.crop.crop_number;
					new_monthly_area_per_crop[crop_number] <- new_monthly_area_per_crop[crop_number] + field.shape.area;
				}			
			}
			return new_monthly_area_per_crop;				
		} else {
			return monthly_crop_area;			

		}
	}	
}

species CorregoRato parent: WaterSource {
	geometry shape <- shape_file('../includes/shapes/cor_rato.shp') as geometry;
	matrix flow_data <- csv_file('../includes/data/flow_rato.csv') as matrix;
	aspect {
		draw shape color: #lightgreen border: #black;
	}
}

species SamambaiaNorte parent: WaterSource {
	geometry shape <- shape_file('../includes/shapes/sam_norte.shp') as geometry;
	matrix flow_data <- csv_file('../includes/data/flow_norte.csv') as matrix;
	aspect {
		draw shape color: #lightyellow border: #black;
	}
}

species SamambaiaSul parent: WaterSource {
	geometry shape <- shape_file('../includes/shapes/sam_sul.shp') as geometry;
	matrix flow_data <- csv_file('../includes/data/flow_sul.csv') as matrix;
	aspect {
		draw shape color: #lightblue border: #black;
	}
}