model farmers_communication

global {
	Participant p;
	
	init {
		create Initiator;
		create Participant returns: ps;
		 p <- ps at 0;
		 write 'Step the simulation to observe the outcome in the console';
	}
}
species Initiator skills: [fipa] {
	reflex send_propose_message {
		write name + ' sends a propose message: Would like to cooperate?';
		do start_conversation to: [p] protocol: 'fipa-propose' performative: 'propose' contents: ['Would like to cooperate?'] ;
	}

	reflex read_accept_proposals when: time = 1 {
		write name + ' receives accept_proposal messages';
		loop i over: accept_proposals {
			write 'accept_proposal message with content: ' + string(i.contents);
		}
	}
	reflex read_reject_proposals when: time = 2 {
		write name + ' receives reject_proposal messages';
		loop i over: reject_proposals {
			write 'reject_proposal message with content: ' + string(i.contents);
		}
	}
}
species Participant skills: [fipa] {
	reflex accept_proposal when: time = 1 {
		message proposalFromInitiator <- proposes at 0;
		
		do accept_proposal message: proposalFromInitiator contents: ['Alright!'] ;
	}
	reflex reject_proposal when: time = 2 {
		message proposalFromInitiator <- proposes at 0;
		
		do reject_proposal message: proposalFromInitiator contents: ['No, thanks.'] ;
	}
}
experiment test_propose_interaction_protocol type: gui {}
