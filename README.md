# Multi-Agents Systems project

This project aims to apply some acquirements from the Multi-Agent Systems (SMA) course taught by professor Célia Ghedini at the University of Brasília (UnB), Brazil, in 2020. This is an update of a previous Agent Based Modeling and Simulation (ABMS) model developed by Pedro Porto and Guido Oliveira in 2019, which aims to analyze water usage for agriculture at the Samambaia river basin in Cristalina municipality, State of Goiás, Brazil. GAMA platform is used with GAML language.

## Getting started

First, you need to download the SMAProject_SamambaiaBasin-ABMS .zip file on your local machine.

### Installing

In order to run the model, you need to install the GAMA Platform <https://gama-platform.github.io/download>

## Opening the project on GAMA
- Open the GAMA program and locate **User models** on the upper-left. 
- With the right button click on *New* > *GAMA Project* and write a name for the project.
-  Then, by clicking with the right button on the Project you have just created, click on *Import* > *External files from disk* and select the .zip file that has just been downloaded.
- Select the GAMA project you have just created.

## Running the tests
Open the *experiments* directory and select *SAMR_mainGui*. Execute the test by clicking on the green button upper-left the file that was just opened.

## Authors
- Guido Dutra
- Pedro Porto
- Déborah Santos