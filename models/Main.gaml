model Basin

import '../species/Crop.gaml'
import '../species/Field.gaml'

global {
	
	float teste;
//	geometry shape;
	geometry shape <- shape_file("../includes/shapes/bacia_samambaia.shp") as geometry; //delimitating simulation area
	float step <- 1 #day;
    date starting_date <- date([2015,12,31]);
 	
	float pumping_cost <- 0.0002;
	
	float water_right_agriculture <- 0.7;	//initial value
	
	list<Crop> crops;
	list<WaterSource> water_sources;
	list global_monthly_data;

	float daily_water_demand update: calculate_daily_water_demand();
	float total_water_demand update: total_water_demand + daily_water_demand;
	
	int year_count update: floor(cycle / 360) as int;
	int month_count <- 0 update: floor(cycle / 30) as int;
	int month_of_year <- 1 update: mod(month_count + 1, 12); // 1 to 12

	list<float> total_production <- list_with(9, 0.0);
	list<float> total_revenue <- list_with(9, 0.0);
	
	action calculate_daily_water_demand type: float {
		return sum(collect(water_sources, each.daily_demand));
	}
	
	float price_tolerance_base <- 0.05;
	float price_tolerance_per_field <- 0.005;
	
	matrix monthly_prices <- csv_file("../includes/data/monthly_prices.csv") as matrix;
	
	matrix daily_weather <- csv_file("../includes/data/daily_weather.csv") as matrix;
	matrix monthly_weather <- csv_file("../includes/data/monthly_weather.csv") as matrix;

	float max_temp update: daily_weather[2, cycle] as float;
	float min_temp update: daily_weather[3, cycle] as float;
	
	float daily_rain update: daily_weather[1, cycle] as float;

	float evapotranspiration update: calculate_evapotranspiration();

	action calculate_evapotranspiration type: float {
		return 0.00017136 * (max_temp - min_temp) ^ (0.96) * (((max_temp + min_temp) / 2) + 17.38);
	}

	init {
		create Soy;
		create Cotton;
		create Corn;
		create Bean;
		create Potato;
		create Garlic;
		create Onion;
		create Tomato;
		add Soy[0] to: crops;
		add Corn[0] to: crops;
		add Cotton[0] to: crops;
		add Bean[0] to: crops;
		add Potato[0] to: crops;
		add Garlic[0] to: crops;
		add Onion[0] to: crops;
		add Tomato[0] to: crops;

		create CorregoRato;
		create SamambaiaNorte;
		create SamambaiaSul;
		add CorregoRato[0] to: water_sources;
		add SamambaiaNorte[0] to: water_sources;
		add SamambaiaSul[0] to: water_sources;
		
		create Field from: shape_file("../includes/shapes/pivos_bacia_2016.shp");
		
		// relate fields to water sources
		loop field over: Field {
			loop water_source over: water_sources {
				if (water_source overlaps field) {
					field.water_source <- water_source;
					add field to: water_source.irrigated_fields;
				}
			}
		}
		
		create Farmer number: 10 { // farmers in corrego do rato
			number_of_fields <- 2;
			price_tolerance <- price_tolerance_base + number_of_fields * price_tolerance_per_field;
			location <- any_location_in(4000 around Field(246));
		}
		
		create Farmer number: 14 { // farmers in samambaia norte
			number_of_fields <- 8;
			price_tolerance <- price_tolerance_base + number_of_fields * price_tolerance_per_field;
			location <- any_location_in(4000 around Field(9));
		}
		
		create Farmer number: 6 { // farmers in samambaia sul
			number_of_fields <- 25;
			price_tolerance <- price_tolerance_base + number_of_fields * price_tolerance_per_field;
			location <- any_location_in(4000 around Field(118));			
		}
		
		// relate farmers to fields
		loop farmer over: Farmer {
			list<Field> empty_fields <- Field where (each.farmer = nil);
			farmer.fields <- empty_fields closest_to (farmer, farmer.number_of_fields);
			farmer.my_area <- sum(collect(farmer.fields, each.field_area)); 
			loop field over: farmer.fields {
				field.farmer <- farmer;
			}
		}
	}

species SAMR{ //Results Monitoring Adaptive System
	
		reflex {
		if (cycle = 0){
			teste <- rnd (100);	
		}
		
		}
	}	
	
	reflex daily_cash_data{
			
		save [ 
			current_date.day,
			current_date.month,
			current_date.year,
			sum(collect(Farmer, each.cash))/ 1000000
			] to: "../results/daily_cash_results.csv" type: "csv" rewrite:false;
			
				
			}		
			
			reflex monthly_cash_data {
		}
	init{
		matrix daily_cash_data <-file("../results/daily_cash_results.csv") as matrix;
		string current_month;
		string current_year;
		string current_month_date;
		float daily_cash_datum;
		float monthly_cash_data;
		string month;
	
		current_month <- daily_cash_data[1,1];
		current_year <- daily_cash_data[2,1];
				
		loop i from: 1 to: 1319 {
			month <- daily_cash_data[1,i];
			
			if month = current_month {
				daily_cash_datum <- daily_cash_data[3, i];
				monthly_cash_data <- monthly_cash_data + daily_cash_datum;
			}else{
				current_month_date <- current_month + '-' + current_year;
							
				save [ 
				current_month_date,
				monthly_cash_data
				] to: "../results/monthly_cash_results.csv" type: "csv" rewrite:false;
					
				monthly_cash_data <- daily_cash_data[3,i];
				current_month <- daily_cash_data[1,i];
				current_year <- daily_cash_data[2,i];
			}
		}	
	} 		
				
	reflex daily_demand_data{
			
		save [ 
			current_date.day,
			current_date.month,
			current_date.year,
			sum(collect(water_sources, each.daily_demand))/ 1000000
			] to: "../results/daily_water_demand_results.csv" type: "csv" rewrite:false;
			
				
			}
			
	reflex monthly_demand_data {
		}
	init{
		matrix daily_demand_data <-file("../results/daily_water_demand_results.csv") as matrix;
		string current_month;
		string current_year;
		string current_month_date;
		float daily_demand_datum;
		float monthly_demand_data;
		string month;
		
		current_month <- daily_demand_data[1,1];
		current_year <- daily_demand_data[2,1];
				
		loop i from: 1 to: 1319 {
			month <- daily_demand_data[1,i];
			
			if month = current_month {
				daily_demand_datum <- daily_demand_data[3, i];
				monthly_demand_data <- monthly_demand_data + daily_demand_datum;
			} else {
				current_month_date <- current_month + '-' + current_year;
			save [ 
			current_month_date,
			monthly_demand_data
			] to: "../results/monthly_demand_results.csv" type: "csv" rewrite:false;
				
			monthly_demand_data <- daily_demand_data[3,i];
			current_month <- daily_demand_data[1,i];
			current_year <- daily_demand_data[2,i];
			}
		}	
	}
		
		reflex daily_supply_data{
			
		save [ 
			current_date.day,
			current_date.month,
			current_date.year,
			sum(collect(water_sources, each.daily_supply))/ 1000000
			] to: "../results/daily_supply_results.csv" type: "csv" rewrite:false;
			
				
			}
			
			reflex monthly_supply_data {
		}
	init{
		matrix daily_supply_data <-file("../results/daily_supply_results.csv") as matrix;
		string current_month;
		string current_year;
		string current_month_date;
		float daily_supply_datum;
		float monthly_supply_data;
		string month;
		
		current_month <- daily_supply_data[1,1];
		current_year <- daily_supply_data[2,1];
				
				
		loop i from: 1 to: 1319 {
			month <- daily_supply_data[1,i];
			
			if month = current_month {
				daily_supply_datum <- daily_supply_data[3, i];
				monthly_supply_data <- monthly_supply_data + daily_supply_datum;
			} else {
				current_month_date <- current_month + '-' + current_year;
							
			save [ 
			current_month_date,
			monthly_supply_data
			] to: "../results/monthly_supply_results.csv" type: "csv" rewrite:false;
				
			monthly_supply_data <- daily_supply_data[3,i];
			current_month <- daily_supply_data[1,i];
			current_year <- daily_supply_data[2,i];
			}
		}	
	}

	reflex end_simulation when: cycle = 1320 { //pause simulation after 10 years
	
		do pause;
		
		}
}








